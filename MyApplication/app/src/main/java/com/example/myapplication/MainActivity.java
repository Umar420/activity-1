package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void toast1(View v){
        Context context=getApplicationContext();
        Toast.makeText(context,"Hello World", Toast.LENGTH_SHORT).show();
    }
    public void toast2(View v){
        Context context=getApplicationContext();
        Toast.makeText(context,"Hello SMD", Toast.LENGTH_SHORT).show();
    }
}